#include "timezone.h"

#include <QDirIterator>
#include <QProcess>
#include <QTimeZone>
#include <stdexcept>

Q_LOGGING_CATEGORY(org_freebsd_timezone, "org.freebsd.timezone");

static const QString zone_files_path = "/usr/share/zoneinfo";
static const QString local_time_file_path = "/etc/localtime";

const std::optional<bool> FreeBSD::Timezone::set_timezone(
    const QString& timezone) {
	std::optional<QString> zone_file_path{zone_file(timezone)};
	if (!zone_file_path) {
		qCDebug(org_freebsd_timezone)
		    << "Time zone" << timezone << "is not available.";
		return {};
	}

	if (QFile::exists(local_time_file_path) &&
	    !QFile::remove(local_time_file_path)) {
		qCDebug(org_freebsd_timezone)
		    << "failed to remove local time file"
		    << local_time_file_path;
		return false;
	}

	QFile zone{zone_file_path.value()};
	if (!zone.link(local_time_file_path)) {
		qCDebug(org_freebsd_timezone)
		    << "failed to link new local time file to " << timezone;
		return false;
	}

	return true;
}

const std::optional<QString> FreeBSD::Timezone::get_timezone() {
	// TODO: should we read and compare /etc/localtime or rely on Qt?
	QString current_timezone{QTimeZone::systemTimeZoneId()};
	if (available(current_timezone)) {
		return current_timezone;
	}
	// TODO: can this even happen?
	return {};
}

bool FreeBSD::Timezone::available(const QString& timezone) {
	return zone_file(timezone).has_value();
}

std::optional<QString> FreeBSD::Timezone::zone_file(const QString& timezone) {
	QMap<QString, QString> zones{available_zones()};
	if (zones.contains(timezone)) {
		return zones[timezone];
	}
	return {};
}

QMap<QString, QString> FreeBSD::Timezone::available_zones() {
	QDir zone_file_dir{zone_files_path};
	QDirIterator zone_files_iter(zone_files_path, {"*"}, QDir::Files,
				     QDirIterator::Subdirectories);

	QMap<QString, QString> result;

	while (zone_files_iter.hasNext()) {
		const QString zone_file_path{zone_files_iter.next()};
		const QString zone_file_name{
		    zone_file_dir.relativeFilePath(zone_file_path)};

		result.insert(zone_file_name, zone_file_path);
	}

	return result;
}
