#pragma once

/* Wrapper for the serivce command */

/* Wrapper for booleans in sysrc */

#include <QLoggingCategory>
#include <QObject>
#include <optional>

Q_DECLARE_LOGGING_CATEGORY(org_freebsd_service);

namespace FreeBSD {

class Service : public QObject {
	Q_OBJECT
       public:
	Service(const QString &service_name);

	const std::optional<bool> start();
	const std::optional<bool> stop();

       private:
	QString m_service_name;

	const std::optional<QString> run(const QStringList &args);
	const void check_validity();
};
};  // namespace FreeBSD
