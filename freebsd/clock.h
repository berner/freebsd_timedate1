#pragma once

#include <QLoggingCategory>
Q_DECLARE_LOGGING_CATEGORY(org_freebsd_clock);

namespace FreeBSD {
class Clock : public QObject {
	Q_OBJECT

       public:
	std::optional<bool> set_time(const qint64 mikro_seconds,
				     const bool relative);

	const bool rtc_is_utc();
	const std::optional<bool> set_rtc_to_utc();
	const std::optional<bool> set_rtc_to_local();
	const std::optional<bool> restart_daemon();
};
};  // namespace FreeBSD
