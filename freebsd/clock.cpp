#include "clock.h"

#include <sys/time.h>
#include <time.h>

#include <QFile>
#include <QProcess>
#include <stdexcept>

Q_LOGGING_CATEGORY(org_freebsd_clock, "org.freebsd.clock");

static const QString cmos_file_path = "/etc/wall_cmos_clock";
static const QString adjkerntz_command = "adjkerntz";

std::optional<bool> FreeBSD::Clock::set_time(const qint64 mikro_seconds,
					     const bool relative) {
	struct timeval new_time;
	new_time.tv_sec = 0;
	new_time.tv_usec = 0;
	if (relative) {
		// TODO: is this correct?
		gettimeofday(&new_time, nullptr);
	}
	new_time.tv_sec += mikro_seconds / 1000000000;
	new_time.tv_usec += mikro_seconds % 1000000000;

	if (settimeofday(&new_time, nullptr) == 0) {
		return true;
	} else {
		qCDebug(org_freebsd_clock)
		    << "system time could not be set to new time to"
		    << mikro_seconds;
		return {};
	}
}

const bool FreeBSD::Clock::rtc_is_utc() {
	return !QFile::exists(cmos_file_path);
}

const std::optional<bool> FreeBSD::Clock::set_rtc_to_local() {
	if (rtc_is_utc()) {
		QFile cmos_file{cmos_file_path};

		if (cmos_file.open(QIODevice::WriteOnly)) {
			return true;
		} else {
			qCDebug(org_freebsd_clock)
			    << "Could not set system clock to local time";
			return {};
		}
	}
	// already local
	return false;
}

const std::optional<bool> FreeBSD::Clock::set_rtc_to_utc() {
	if (!rtc_is_utc()) {
		QFile cmos_file{cmos_file_path};

		if (cmos_file.remove()) {
			return true;
		} else {
			qCDebug(org_freebsd_clock)
			    << "Could not set system clock to UTC";
			return {};
		}
	}
	// already UTC
	return false;
}

const std::optional<bool> FreeBSD::Clock::restart_daemon() {
	QProcess adjkerntz;
	// TODO: Is this correct?
	adjkerntz.start(adjkerntz_command, QStringList() << "-a");

	if (!adjkerntz.waitForFinished()) {
		qCDebug(org_freebsd_clock)
		    << "Could not call adjkerntz (timeout)";
		return {};
	}
	if (adjkerntz.exitStatus() != QProcess::ExitStatus::NormalExit) {
		qCDebug(org_freebsd_clock)
		    << "Failed to run adjkerntz (failed)";
		return {};
	}

	return true;
}
