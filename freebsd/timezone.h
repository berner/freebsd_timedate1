#pragma once

#include <QLoggingCategory>
#include <QMap>
#include <QObject>

Q_DECLARE_LOGGING_CATEGORY(org_freebsd_timezone);

namespace FreeBSD {
class Timezone : public QObject {
	Q_OBJECT
       public:
	const std::optional<bool> set_timezone(const QString &timezone);
	const std::optional<QString> get_timezone();

	static bool available(const QString &timezone);
	static std::optional<QString> zone_file(const QString &timezone);
	static QMap<QString, QString> available_zones();
};
};  // namespace FreeBSD
