#include "sysrcbool.h"

#include <QProcess>
#include <stdexcept>

static const QString syrsrc_command = "sysrc";
Q_LOGGING_CATEGORY(org_freebsd_sysrcbool, "org.freebsd.sysrcbool");

FreeBSD::SysrcBool::SysrcBool(const QString &sysrc_name)
    : m_sysrc_name{sysrc_name} {
	check_validity();
}

const std::optional<bool> FreeBSD::SysrcBool::getState() {
	return run({"-n", m_sysrc_name}) == "YES";
}

const std::optional<bool> FreeBSD::SysrcBool::setState(const bool new_state) {
	if (run({m_sysrc_name + "=" + (new_state ? "YES" : "NO")})) {
		return new_state;
	}
	return {};
}

const std::optional<QString> FreeBSD::SysrcBool::run(const QStringList &args) {
	QProcess sysrc;
	sysrc.start(syrsrc_command, args);

	if (!sysrc.waitForFinished()) {
		qCDebug(org_freebsd_sysrcbool)
		    << m_sysrc_name << "Could not call sysrc for (timeout)";
		return {};
	}

	if (sysrc.exitStatus() != QProcess::ExitStatus::NormalExit) {
		qCDebug(org_freebsd_sysrcbool)
		    << m_sysrc_name << "Could not call sysrc for (failed)";
		return {};
	}

	return sysrc.readAll().trimmed();
}

const void FreeBSD::SysrcBool::check_validity() {
	std::optional<QString> available_bools{run({"-aN"})};
	bool found{false};
	if (available_bools) {
		for (const auto &service :
		     available_bools.value().split('\n')) {
			if (service == m_sysrc_name) {
				found = true;
				break;
			}
		}
	}

	if (!found) {
		QString error{"No such sysrc '" + m_sysrc_name + "'."};
		qCDebug(org_freebsd_sysrcbool()) << error;
		throw std::runtime_error(error.toLocal8Bit());
	}
}
