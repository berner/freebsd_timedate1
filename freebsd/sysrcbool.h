#pragma once

/* Wrapper for booleans in sysrc */

#include <QLoggingCategory>
#include <QObject>
#include <optional>

Q_DECLARE_LOGGING_CATEGORY(org_freebsd_sysrcbool);

namespace FreeBSD {

class SysrcBool : public QObject {
	Q_OBJECT
       public:
	SysrcBool(const QString &sysrc_name);

	const std::optional<bool> setState(const bool new_state);
	const std::optional<bool> getState();

       private:
	QString m_sysrc_name;

	const std::optional<QString> run(const QStringList &args);

	const void check_validity();
};
};  // namespace FreeBSD
