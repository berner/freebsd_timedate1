#include "service.h"

#include <QProcess>
#include <stdexcept>

static const QString service_command = "service";

Q_LOGGING_CATEGORY(org_freebsd_service, "org.freebsd.service");

FreeBSD::Service::Service(const QString &service_name)
    : m_service_name{service_name} {
	check_validity();
}

const std::optional<bool> FreeBSD::Service::start() {
	if (run({m_service_name, "start"})) {
		return true;
	}
	return {};
}

const std::optional<bool> FreeBSD::Service::stop() {
	if (run({m_service_name, "stop"})) {
		return true;
	}
	return {};
}

const std::optional<QString> FreeBSD::Service::run(const QStringList &args) {
	QProcess service;
	service.start(service_command, args);

	if (!service.waitForFinished()) {
		qCDebug(org_freebsd_service)
		    << m_service_name << "Could not call service for (timeout)";
		return {};
	}

	if (service.exitStatus() != QProcess::ExitStatus::NormalExit) {
		qCDebug(org_freebsd_service)
		    << m_service_name << "Could not call service for (failed)";
		return {};
	}

	return service.readAll().trimmed();
}

const void FreeBSD::Service::check_validity() {
	std::optional<QString> available_services{run({"-l"})};
	bool found{false};
	if (available_services) {
		for (const auto &service :
		     available_services.value().split('\n')) {
			if (service == m_service_name) {
				found = true;
				break;
			}
		}
	}

	if (!found) {
		QString error{"No such sysrc '" + m_service_name + "'."};
		qCDebug(org_freebsd_service) << error;
		throw std::runtime_error(error.toLocal8Bit());
	}
}

