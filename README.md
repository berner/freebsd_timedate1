Prototype of an implementation of the timdate1 DBus service [1]

The current implementation exposes the following interface:
```
node /org/freedesktop/timedate1 {
  interface org.freedesktop.timedate1 {
    methods:
      SetNTP(in  b use_ntp,
             in  b user_interaction);
      SetLocalRTC(in  b local_rtc,
                  in  b fix_system,
                  in  b user_interaction);
      SetTime(in  x usec_utc,
              in  b relative,
              in  b user_interaction);
      SetTimezone(in  s timezone,
                  in  b user_interaction);
    signals:
    properties:
      readonly s Timezone = 'Europe/Zurich';
      readonly b NTP = true;
      readonly b LocalRTC = true;
  };
  interface org.freedesktop.DBus.Properties {
    methods:
      Get(in  s interface_name,
          in  s property_name,
          out v value);
      Set(in  s interface_name,
          in  s property_name,
          in  v value);
      @org.qtproject.QtDBus.QtTypeName.Out0("QVariantMap")
      GetAll(in  s interface_name,
             out a{sv} values);
    signals:
      @org.qtproject.QtDBus.QtTypeName.Out1("QVariantMap")
      PropertiesChanged(s interface_name,
                        a{sv} changed_properties,
                        as invalidated_properties);
    properties:
  };
  interface org.freedesktop.DBus.Introspectable {
    methods:
      Introspect(out s xml_data);
    signals:
    properties:
  };
  interface org.freedesktop.DBus.Peer {
    methods:
      Ping();
      GetMachineId(out s machine_uuid);
    signals:
    properties:
  };
};
```

Note: clock setting is untested, and probably wrong :)


[1] https://man7.org/linux/man-pages/man5/org.freedesktop.timedate1.5.html
