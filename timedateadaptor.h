#pragma once

#include <QDBusAbstractAdaptor>
#include "freebsd/sysrcbool.h"
#include "freebsd/service.h"
#include "freebsd/clock.h"
#include "freebsd/timezone.h"

class QCoreApplication;

class TimeDateAdaptor : public QDBusAbstractAdaptor {
	Q_OBJECT
	Q_CLASSINFO("D-Bus Interface", "org.freedesktop.timedate1")

	// <property name="Timezone" type="s" access="read"/>
	Q_PROPERTY(QString Timezone READ getTimezone)

	// <property name="NTP" type="b" access="read"/>
	Q_PROPERTY(bool NTP READ getNTP)

	// <property name="LocalRTC" type="b" access="read"/>
	Q_PROPERTY(bool LocalRTC READ getLocalRTC)

       public slots:
	//<method name="SetNTP">< arg *direction="in" type="b"
	//name="use_ntp"/><arg direction="in" type="b"
	//name="user_interaction"/></method>
	void SetNTP(bool use_ntp, bool user_interaction);

	//<method name="SetLocalRTC"><arg direction="in" type="b"
	//name="local_rtc"/><arg direction="in" type="b" name="fix_system"/><arg
	//direction="in" type="b" name="user_interaction"/></method>
	void SetLocalRTC(bool local_rtc, bool fix_system,
			 bool user_interaction);

	//  <method name="SetTime"><arg direction="in" type="x"
	//  name="usec_utc"/><arg direction="in" type="b" name="relative"/><arg
	//  direction="in" type="b" name="user_interaction"/></method>
	void SetTime(qint64 usec_utc, bool relative, bool user_interaction);

	// <method name="SetTimezone"><arg direction="in" type="s"
	// name="timezone"/><arg direction="in" type="b"
	// name="user_interaction"/></method>
	void SetTimezone(QString timezone, bool user_interaction);

       private:
	QCoreApplication *app;

       public:
	TimeDateAdaptor(QCoreApplication *application);
	QString getTimezone();
	bool getNTP();
	bool getLocalRTC();

    // System wrappers
    FreeBSD::SysrcBool m_ntpd_sysrc;
    FreeBSD::Service   m_ntpd_service;
    FreeBSD::Timezone  m_timezone;
    FreeBSD::Clock     m_clock;
};
