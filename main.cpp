#include <QDBusConnection>
#include <QtCore/QCoreApplication>

#include "timedateadaptor.h"

#include "freebsd/sysrcbool.h"

int main(int argc, char **argv) {
	QCoreApplication::setSetuidAllowed(true);
	QCoreApplication app(argc, argv);

	new TimeDateAdaptor(&app);

	QDBusConnection::systemBus().registerService(
	    "org.freedesktop.timedate1");
	QDBusConnection::systemBus().registerObject(
	    "/org/freedesktop/timedate1", &app);


	app.exec();
}
