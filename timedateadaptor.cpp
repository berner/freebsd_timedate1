#include "timedateadaptor.h"

#include <QCoreApplication>

static const QString ntpd_sysrc = "ntpd_enable";
static const QString ntpd_service = "ntpd";

TimeDateAdaptor::TimeDateAdaptor(QCoreApplication *application)
    : QDBusAbstractAdaptor(application),
    app(application),
    m_ntpd_sysrc{ntpd_sysrc},
    m_ntpd_service{ntpd_service},
    m_timezone{},
    m_clock{} {}

QString TimeDateAdaptor::getTimezone() {
	return m_timezone.get_timezone().value_or("");
}

void TimeDateAdaptor::SetTimezone(QString timezone, bool user_interaction) {
    m_timezone.set_timezone(timezone);
}

bool TimeDateAdaptor::getNTP() {
    return m_ntpd_sysrc.getState().value_or(false);
}

void TimeDateAdaptor::SetNTP(bool use_ntp, bool user_interaction) {
    if (m_ntpd_sysrc.setState(use_ntp)) {
        if (use_ntp) {
            m_ntpd_service.start();
        } else {
            m_ntpd_service.stop();
        }
    }
}

bool TimeDateAdaptor::getLocalRTC() {
    return !m_clock.rtc_is_utc();
}

void TimeDateAdaptor::SetLocalRTC(bool local_rtc, bool fix_system,
				  bool user_interaction) {

    std::optional<bool> rtc_update { local_rtc ?  m_clock.set_rtc_to_local() : m_clock.set_rtc_to_utc() };

    if (rtc_update.value_or(false) && fix_system) {
        m_clock.restart_daemon();
    }
}

void TimeDateAdaptor::SetTime(qint64 usec_utc, bool relative,
			      bool user_interaction) {
    m_clock.set_time(usec_utc, relative);
}

